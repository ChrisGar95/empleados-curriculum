// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    
    apiKey: "AIzaSyBDpCHW88COE60eyeZazamk1K9D614QyVY",
    authDomain: "empleos1-2020.firebaseapp.com",
    databaseURL: "https://empleos1-2020.firebaseio.com",
    projectId: "empleos1-2020",
    storageBucket: "empleos1-2020.appspot.com",
    messagingSenderId: "22278418164",
    appId: "1:22278418164:web:661914baaeb87b732df8a1"
  },
  googleWebClientId: '22278418164-pokliff9brvrnseq9nujou07g74cgmvv.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
