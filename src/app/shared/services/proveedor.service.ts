import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Respuesta } from '../model/respuesta';
import { Proovedor } from '../model/proveedor';

@Injectable({
  providedIn: 'root'
})
export class ConsultorioService {

  private SEND_PROOVEDOR_URL:string = 'http://localhost:8090/PacientesWebServer/server/proveedores/insertar';
  private GET_PROOVEDOR_URL:string = 'http://localhost:8090/PacientesWebServer/server/proveedores/listado';

  constructor(private http: HttpClient) { }

  sendProovedor(proovedor: Proovedor): Observable<any> {
    return this.http.post<Respuesta>(this.SEND_PROOVEDOR_URL, proovedor);
  }

  getProovedores(): Observable<any[]> {
    return this.http.get<Proovedor[]>(this.GET_PROOVEDOR_URL);
  }
}
