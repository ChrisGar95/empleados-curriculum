import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../model/usuario';
import { Observable } from 'rxjs';
import { Respuesta } from '../model/respuesta';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private SEND_USUARIO_URL:string = 'http://localhost:8090/PacientesWebServer/server/usuarios/listado';
  private GET_USUARIO_URL:string = 'http://localhost:8090/PacientesWebServer/server/usuarios/listado';

  constructor(private http: HttpClient) { }

  sendUsuario(usuario: Usuario): Observable<any> {
    return this.http.post<Respuesta>(this.SEND_USUARIO_URL, usuario);
  }

  getUsuarios(): Observable<any[]> {
    return this.http.get<Usuario[]>(this.GET_USUARIO_URL);
  }

}
