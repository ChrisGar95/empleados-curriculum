export class Proovedor {
    cedula: string;
    correo: string;
    direccion: string;
    nombre: string;
    telefono: string;
}