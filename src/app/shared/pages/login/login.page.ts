import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public router: Router,private googlePlus: GooglePlus,private auth: AuthService) { }

  ngOnInit() {
  }

  async loginGoogle(){
    const error = await this.auth.googleLogin();
    if(error==undefined){
      this.router.navigate(['menu']);
    }
    else{
      alert(JSON.stringify(error));
    }
  }

  register(){
    this.router.navigate(['register'])
  }

}
