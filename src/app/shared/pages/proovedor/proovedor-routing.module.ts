import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProovedorPage } from './proovedor.page';

const routes: Routes = [
  {
    path: '',
    component: ProovedorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProovedorPageRoutingModule {}
