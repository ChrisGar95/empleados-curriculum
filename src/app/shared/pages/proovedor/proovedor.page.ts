import { Component, OnInit } from '@angular/core';
import { Proovedor } from '../../model/proveedor';
import { Observable } from 'rxjs';
import { ConsultorioService } from '../../services/proveedor.service';

@Component({
  selector: 'app-proovedor',
  templateUrl: './proovedor.page.html',
  styleUrls: ['./proovedor.page.scss'],
})
export class ProovedorPage implements OnInit {

  proovedores: Observable<Proovedor[]>;

  constructor( private consultorioService : ConsultorioService) { }

  ngOnInit() {
    this.proovedores = this.consultorioService.getProovedores();
  }

}
