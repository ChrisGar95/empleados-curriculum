import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProovedorPage } from './proovedor.page';

describe('ProovedorPage', () => {
  let component: ProovedorPage;
  let fixture: ComponentFixture<ProovedorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProovedorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProovedorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
