import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProovedorPageRoutingModule } from './proovedor-routing.module';

import { ProovedorPage } from './proovedor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProovedorPageRoutingModule
  ],
  declarations: [ProovedorPage]
})
export class ProovedorPageModule {}
