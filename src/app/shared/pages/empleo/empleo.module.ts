import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmpleoPageRoutingModule } from './empleo-routing.module';

import { EmpleoPage } from './empleo.page';
import { ImageUploadComponent } from 'src/app/components/image-upload/image-upload.component';
import { TestcomponentComponent } from 'src/app/components/testcomponent/testcomponent.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmpleoPageRoutingModule
  ],
  declarations: [EmpleoPage, ImageUploadComponent, TestcomponentComponent],
  exports: [ImageUploadComponent, TestcomponentComponent]
})
export class EmpleoPageModule {}
