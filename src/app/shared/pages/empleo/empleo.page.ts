import { Component, OnInit } from '@angular/core';
import { EmpleosService } from '../../services/empleos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Empleo } from '../../model/empleo';
import { Camera } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-empleo',
  templateUrl: './empleo.page.html',
  styleUrls: ['./empleo.page.scss'],
})
export class EmpleoPage implements OnInit {

  /*Cuando quiero utilizar con suscribe
  empleo: Observable<any>
  empleoUpdate: Empleo = new Empleo()*/
  
  //Cuando utilizo con el await
  empleo: Empleo = new Empleo()
 
  //icono:string='camera';
  icono:string='geolocalizacion';

  constructor(public router: Router, private empleosService: EmpleosService, public alertController: AlertController, private route: ActivatedRoute, private camera: Camera) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id")
    
    /*------------Una forma de obtener un objeto manipulable--------//
    this.empleo = this.empleosService.getEmpleo(id)
    this.empleo.subscribe(data => {this.empleoUpdate=data;})//
    //-------------------------------------------------------------*/

    //------------Otra Forma de obtener un objeto manipulable-----//
    this.empleo = await this.empleosService.getEmpleoById(id);
    
    //-----------------------------------------------------------//
    
  }

  actualizaEmpleo()
  {
    /*Cuando utilizo con el suscribe
    this.empleosService.saveEmpleo(this.empleoUpdate);*/

    this.empleosService.saveEmpleo(this.empleo);
    
    this.router.navigate(['lista-empleos']);
    
  }

  imagenCargada(e){
    this.empleo.image = e;
    this.presentAlert();
  }

  geolocaliza(e){
    console.log("hecho");
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Hecho',
      message: 'Imagen Subida',
      buttons: ['OK']
    });

    await alert.present();
  }

}
