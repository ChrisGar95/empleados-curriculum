import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios.service';
import { Usuario } from '../../model/usuario';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.page.html',
  styleUrls: ['./lista-usuarios.page.scss'],
})
export class ListaUsuariosPage implements OnInit {
  usuarios: Observable<Usuario[]>;

  constructor(private usuarioService: UsuariosService) { }

  ngOnInit() {
    this.usuarios = this.usuarioService.getUsuarios();
  }

}
