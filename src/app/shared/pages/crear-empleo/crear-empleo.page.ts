import { Component, OnInit } from '@angular/core';
import { Empleo } from '../../model/empleo';
import { EmpleosService } from '../../services/empleos.service';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-crear-empleo',
  templateUrl: './crear-empleo.page.html',
  styleUrls: ['./crear-empleo.page.scss'],
})
export class CrearEmpleoPage implements OnInit {

  empleo: Empleo = new Empleo()
  base64Image: any;

  icono:string='camera';

  constructor(public router: Router,private empleosService: EmpleosService,private camera: Camera, public alertController: AlertController) { }

  ngOnInit() {
  }

  saludar(data){
    console.log(data);
  }

  imagenCargada(e){
    this.empleo.image = e;
    this.presentAlert();
  }

  guardarEmpleo(e){
    //console.log(this.empleo)
    this.empleosService.saveEmpleo(this.empleo);
    this.router.navigate(['lista-empleos']);
    
  }

  tomarFoto()
  {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.base64Image = 'data:image/jpeg;base64,' + imageData;
     console.log(this.base64Image);
    }, (err) => {
     // Handle error
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Hecho',
      message: 'Imagen Subida',
      buttons: ['OK']
    });

    await alert.present();
  }

  

}
