import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Person } from '../interfaces/person';

@Component({
  selector: 'app-experiencia',
  templateUrl: './experiencia.page.html',
  styleUrls: ['./experiencia.page.scss'],
})
export class ExperienciaPage implements OnInit {

  person: Person;

  constructor(public route: ActivatedRoute,public router: Router) { }

  Ingresar()
  {
    
    let extras: NavigationExtras = {
      state:{
        person: this.person
      }
    };
    
    this.router.navigate(['estudios'], extras);
  }

  ngOnInit() {

  this.route.queryParams.subscribe(params => {
    if(this.router.getCurrentNavigation().extras.state){
      this.person = this.router.getCurrentNavigation().extras.state.person;
    }
  });

  }

}
