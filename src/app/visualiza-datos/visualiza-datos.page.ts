import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Person } from '../interfaces/person';

@Component({
  selector: 'app-visualiza-datos',
  templateUrl: './visualiza-datos.page.html',
  styleUrls: ['./visualiza-datos.page.scss'],
})
export class VisualizaDatosPage implements OnInit {

  person: Person;

  constructor(public route: ActivatedRoute,public router: Router) { }

  Regresar()
  {
    
    let extras: NavigationExtras = {
      state:{
        person: this.person
      }
    };
    
    this.router.navigate(['menu']);
  }

  ngOnInit() {

  this.route.queryParams.subscribe(params => {
    if(this.router.getCurrentNavigation().extras.state){
      this.person = this.router.getCurrentNavigation().extras.state.person;
    }
  });

  }

}
