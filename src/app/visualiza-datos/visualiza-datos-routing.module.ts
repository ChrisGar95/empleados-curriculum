import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualizaDatosPage } from './visualiza-datos.page';

const routes: Routes = [
  {
    path: '',
    component: VisualizaDatosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualizaDatosPageRoutingModule {}
