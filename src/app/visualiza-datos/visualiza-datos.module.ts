import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisualizaDatosPageRoutingModule } from './visualiza-datos-routing.module';

import { VisualizaDatosPage } from './visualiza-datos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisualizaDatosPageRoutingModule
  ],
  declarations: [VisualizaDatosPage]
})
export class VisualizaDatosPageModule {}
