import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisualizaDatosPage } from './visualiza-datos.page';

describe('VisualizaDatosPage', () => {
  let component: VisualizaDatosPage;
  let fixture: ComponentFixture<VisualizaDatosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizaDatosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisualizaDatosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
