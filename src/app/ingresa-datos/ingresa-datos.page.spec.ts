import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IngresaDatosPage } from './ingresa-datos.page';

describe('IngresaDatosPage', () => {
  let component: IngresaDatosPage;
  let fixture: ComponentFixture<IngresaDatosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresaDatosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IngresaDatosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
