import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngresaDatosPageRoutingModule } from './ingresa-datos-routing.module';

import { IngresaDatosPage } from './ingresa-datos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IngresaDatosPageRoutingModule
  ],
  declarations: [IngresaDatosPage]
})
export class IngresaDatosPageModule {}
