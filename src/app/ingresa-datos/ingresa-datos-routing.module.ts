import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresaDatosPage } from './ingresa-datos.page';

const routes: Routes = [
  {
    path: '',
    component: IngresaDatosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngresaDatosPageRoutingModule {}
