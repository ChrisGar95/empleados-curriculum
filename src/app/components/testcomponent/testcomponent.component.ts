import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator/ngx';

import { AlertController } from '@ionic/angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'testcomponent',
  templateUrl: './testcomponent.component.html',
  styleUrls: ['./testcomponent.component.scss'],
})
export class TestcomponentComponent implements OnInit {

  //declaro una variable de entrada
  @Input() icon: string;
  @Output() finish = new EventEmitter<any>();

  lat:number
  lon:number
  total:string


  constructor(public alertController: AlertController,public navCtrl: NavController, 
    public geolocation: Geolocation, private launchNavigator:LaunchNavigator) { }

  ngOnInit() {
    
  }

  getGeolocation(){
    this.geolocation.getCurrentPosition().then((geoposition: Geoposition)=>{
      this.lat = geoposition.coords.latitude;
      this.lon = geoposition.coords.longitude;

      let latMadrid = 40.4167;
      let lonMadrid = -3.70325;

      this.total = this.calculateDistance(this.lon, lonMadrid, this.lat, latMadrid)+"KM";
      this.presentAlert(this.lat,this.lon,this.total);
    });
  }

  calculateDistance(lon1, lon2, lat1, lat2){
    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((lat1-lat2) * p) / 2 + c(lat2 * p) *c((lat1) * p) * (1 - c(((lon1- lon2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a)));
    return Math.trunc(dis);
}

  async presentAlert(latitud:number,longitud:number,distancia:string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Ubicacion',
      subHeader: 'Latitud: '+latitud+" , Longitud: "+longitud,
      message: "Distancia a Madrid: "+distancia,
      buttons: ['OK']
    });

    await alert.present();
  }



}
