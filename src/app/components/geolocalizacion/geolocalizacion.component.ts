import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { NavController } from '@ionic/angular';
import { LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator/ngx';

declare var google;

@Component({
  selector: 'app-geolocalizacion',
  templateUrl: './geolocalizacion.component.html',
  styleUrls: ['./geolocalizacion.component.scss'],
})
export class GeolocalizacionComponent implements OnInit {
  @Input() icon: string;
  @Output() finish = new EventEmitter<any>();
  
  latitude:number ;
  longitude:number ;
  
  map: any;

  constructor(public navCtrl: NavController, public geolocation: Geolocation, private launchNavigator:LaunchNavigator) { }

  ngOnInit() {}

  /*ionViewDidLoad(){
    this.geolocation.getCurrentPosition().then(position =>{
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
      },error=>{
          console.log('error',error);
      });
  }

  navigateLocation(){
    let options: LaunchNavigatorOptions = {
     app: this.launchNavigator.APP.GOOGLE_MAPS,
              start:[this.latitude,this.longitude]
       };
   this.launchNavigator.navigate('London, ON',options)
   .then(success =>{
     console.log(success);
   },error=>{
     console.log(error);
   })
 }*/




 ionViewDidLoad(){
  this.getPosition();
}

getPosition():any{
  this.geolocation.getCurrentPosition()
  .then(response => {
    this.loadMap(response);
  })
  .catch(error =>{
    console.log(error);
  })
}

loadMap(position: Geoposition){
  let latitude = position.coords.latitude;
  let longitude = position.coords.longitude;
  console.log(latitude, longitude);
  
  // create a new map by passing HTMLElement
  let mapEle: HTMLElement = document.getElementById('map');

  // create LatLng object
  let myLatLng = {lat: latitude, lng: longitude};

  // create map
  this.map = new google.maps.Map(mapEle, {
    center: myLatLng,
    zoom: 12
  });

  google.maps.event.addListenerOnce(this.map, 'idle', () => {
    let marker = new google.maps.Marker({
      position: myLatLng,
      map: this.map,
      title: 'Hello World!'
    });
    mapEle.classList.add('show-map');
  });
}

}
