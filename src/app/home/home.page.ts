import { Component } from '@angular/core';
import { Person } from '../interfaces/person';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  sexoMasculino: boolean;
  sexoFemenino: boolean;
  experiencia: boolean;

  person : Person = {
    cedula : '',
    nombre : '',
    direccion : '',
    fechaNacimiento :'',
    sexo: '',
    experiencia: '',
    proyeccion: '',
    titulo3Nivel: '',
    titulo4Nivel: ''
  } 

  
  constructor(public router: Router) {}

  Ingresar(){
    //console.log("Cedula "+ this.person.cedula)
    
    let extras: NavigationExtras = {
      state:{
        person: this.person
      }
    };

    if(this.experiencia == true)
    {
      this.router.navigate(['experiencia'], extras);
    }
    else{
      
      this.router.navigate(['proyeccion'], extras);
    }

    
  }

  saludar2(){
    let extras: NavigationExtras = {
      state:{
        person: this.person,
        cedula:"0122",
        id:this.person.cedula
      }
    };
    this.router.navigate(['experiencia'], extras);
  }

  verificaSexo() 
  {

    if(this.sexoMasculino == true)
    {

      this.person.sexo = "masculino"

    }
    else{
        this.person.sexo = "femenino"
    }
   
  }

}
